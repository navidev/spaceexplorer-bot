package extensions

import com.google.gson.JsonElement

fun JsonElement.toStringNoQuotes() : String {
    return this.toString().substringAfter('"').substringBefore('"')
}