package extensions

fun String?.noQuotes() : String {
    return when (this != null) {
        true -> this.substringAfter('"').substringBefore('"')
        false -> ""
    }
}