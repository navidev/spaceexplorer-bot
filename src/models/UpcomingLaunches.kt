package models

import com.google.gson.JsonArray

data class UpcomingLaunches (
    val count: Int,
    val next: String,
    val previous: String,
    val results: JsonArray
)