package models

import com.google.gson.JsonArray
import com.google.gson.JsonObject

data class Spacecraft (
    val id: Int,
    val url: String,
    val name: String,
    val serial_number: String,
    val status: JsonObject,
    val description: String,
    val spacecraft_config: JsonObject,
    val flights: JsonArray
)