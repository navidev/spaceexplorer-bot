package models

import com.google.gson.JsonArray

data class SearchResult (
    val count: Int,
    val next: String,
    val previous: String,
    val results: JsonArray
)