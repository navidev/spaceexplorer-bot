package models

import com.google.gson.JsonArray

data class Agency (
    val id: Int,
    val url: String,
    val name: String,
    val featured: Boolean,
    val type: String,
    val country_code: String,
    val abbrev: String,
    val description: String,
    val administrator: String,
    val founding_year: String,
    val launchers: String,
    val spacecraft: String,
    val parent: String,
    val launch_library_url: String,
    val successful_launches: Int,
    val failed_launches: Int,
    val pending_launches: Int,
    val info_url: String,
    val wiki_url: String,
    val logo_url: String,
    val image_url: String,
    val nation_url: String,
    val launcher_list: JsonArray,
    val spacecraft_list: JsonArray
)