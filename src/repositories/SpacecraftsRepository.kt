package repositories

import com.google.gson.Gson
import conf.spaceAPIConf
import models.SearchResult
import models.Spacecraft
import java.net.URL

class SpacecraftsRepository {

    fun findOneByID(ID: String) : Spacecraft {
        val apiVersion = spaceAPIConf["API_VERSION"]
        val url = URL("https://spacelaunchnow.me/api/$apiVersion/spacecraft/$ID/?format=json").readText()
        val gson = Gson()

        return gson.fromJson(url, Spacecraft::class.java)
    }

//    fun searchByName(name: String) : SearchResult {
//        val apiVersion = spaceAPIConf["API_VERSION"]
//        val url = URL("https://spacelaunchnow.me/api/$apiVersion/spacecraft/?format=json&search=$name").readText()
//        val gson = Gson()
//
//        return gson.fromJson(url, SearchResult::class.java)
//    }
}