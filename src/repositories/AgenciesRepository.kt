package repositories

import com.google.gson.Gson
import conf.spaceAPIConf
import models.Agency
import models.SearchResult
import java.net.URL

class AgenciesRepository {

    fun findOneByID(ID: String) : Agency {
        val apiVersion = spaceAPIConf["API_VERSION"]
        val url = URL("https://spacelaunchnow.me/api/$apiVersion/agencies/$ID/?format=json").readText()
        val gson = Gson()

        return gson.fromJson(url, Agency::class.java)
    }

    fun searchByName(name: String) : SearchResult {
        val apiVersion = spaceAPIConf["API_VERSION"]
        val url = URL("https://spacelaunchnow.me/api/$apiVersion/agencies/?format=json&search=$name").readText()
        val gson = Gson()

        return gson.fromJson(url, SearchResult::class.java)
    }
}