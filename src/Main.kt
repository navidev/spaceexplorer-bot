import com.jagrosh.jdautilities.command.CommandClientBuilder
import com.jagrosh.jdautilities.commons.waiter.EventWaiter
import commands.*
import conf.botTokens
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.entities.Game

class Main {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            initBot()
        }

        private fun initBot() {
            val waiter                  = EventWaiter()
            val commandClientBuilder    = CommandClientBuilder()

            commandClientBuilder.setOwnerId("01010101010101")
                .setPrefix("\$explore ")
                .setGame(Game.watching("\$explore help"))
                .addCommands(
                    AgenciesCommand(),
                    AgencyShowCommand(),
                    SpacecraftShowCommand(),
                    UpcomingLaunchesCommand(),
                    SearchCommand()
                )

            JDABuilder(AccountType.BOT)
                .setToken(botTokens["PROD"])
                .addEventListener(waiter)
                .addEventListener(commandClientBuilder.build())
                .buildAsync()

            println("-> BOT INITIALIZED!")
        }
    }
}