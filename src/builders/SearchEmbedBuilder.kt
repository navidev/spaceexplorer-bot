package builders

import com.google.gson.JsonArray
import models.SearchResult
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color

abstract class SearchEmbedBuilder {

    abstract val model: SearchResult
    abstract val color: Color
    abstract val args: String

    abstract fun makeEmbed() : MessageEmbed
    abstract fun formatStringFromJsonArray(jsonArray: JsonArray) : String
}