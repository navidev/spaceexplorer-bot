package builders

import com.google.gson.JsonArray
import models.SearchResult
import extensions.noQuotes
import extensions.toStringNoQuotes
import models.Agency
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.entities.MessageEmbed
import repositories.AgenciesRepository
import java.awt.Color
import java.lang.StringBuilder

class SearchAgencyEmbedBuilder (searchResultModel: SearchResult, embedColor: Color, args: String) : SearchEmbedBuilder() {

    override val model = searchResultModel
    override val color = embedColor
    override val args = args

    override fun makeEmbed () : MessageEmbed {
        val builder = EmbedBuilder()

        when (model.results.size() > 1) {
            true -> {
                for (i in 0 until (model.results.size())) {

                    val result = model.results[i]
                    val agencyName = result.asJsonObject["name"].toStringNoQuotes().toUpperCase()
                    val agencyID = result.asJsonObject["id"]

                    builder.setTitle(":mag: - ${model.results.size()} RESULTATS POUR \"$args\" ")
                        .setColor(color)
                        .addBlankField(false)
                        .addField(":small_orange_diamond: $agencyName", "Pour voir l'agence, tape `` \$explore agence $agencyID ``\n", false)
                }
                return builder.build()
            }
            false -> {
                val result = model.results[0]
                val id = result.asJsonObject["id"].toString()
                val agencyModel: Agency = AgenciesRepository().findOneByID(id)

                val spaceCraftsString = if (agencyModel.spacecraft_list.size() > 0) {
                    formatStringFromJsonArray(agencyModel.spacecraft_list)
                } else { "Pas d'informations" }

                val launchersString = if (agencyModel.launcher_list.size() > 0) {
                    formatStringFromJsonArray(agencyModel.launcher_list)
                } else { "Pas d'informations" }

                builder.setTitle(agencyModel.name.noQuotes().toUpperCase())
                    .setThumbnail(agencyModel.logo_url.noQuotes())
                    .setImage(agencyModel.image_url.noQuotes())
                    .setColor(color)
                    .addField("", agencyModel.description.noQuotes(), false)
                    .addBlankField(false)
                    .addField("Administrateur(s)", agencyModel.administrator.noQuotes(), true)
                    .addField("Type", agencyModel.type.noQuotes(), true)
                    .addField("Année de création", agencyModel.founding_year.noQuotes(), false)
                    .addBlankField(true)
                    .addField("Lanceurs", launchersString, false)
                    .addField("Vaisseaux", spaceCraftsString, false)
                    .addBlankField(true)
                    .addField("Tirs reussis", agencyModel.successful_launches.toString(), false)
                    .addField("Tirs ratés", agencyModel.failed_launches.toString(), false)
                    .addField("Tirs en attente", agencyModel.pending_launches.toString(), false)
                    .addBlankField(true)
                    .addField("Wiki", agencyModel.wiki_url, true)
                    .addField("Plus d'informations", agencyModel.info_url, true)
                    .addBlankField(true)

                return builder.build()
            }
        }
    }

    override fun formatStringFromJsonArray(jsonArray: JsonArray) : String {
        val builder = StringBuilder()

        for (i in 0 until jsonArray.size()) {
            val spacecraft = jsonArray[i].asJsonObject
            val spaceCraftName = spacecraft["name"]
            val spacecraftID = spacecraft["id"]

            builder.append("[$spacecraftID] - ${spaceCraftName.toStringNoQuotes()}")
            builder.append("\n")
        }

        return builder.toString()
    }

}