package commands

import com.google.gson.JsonArray
import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import extensions.noQuotes
import extensions.toStringNoQuotes
import net.dv8tion.jda.core.EmbedBuilder
import repositories.AgenciesRepository
import java.io.FileNotFoundException
import java.lang.StringBuilder


class AgencyShowCommand : Command() {

    init {
        this.name       = "agence"
        this.aliases    = arrayOf("agc")
        this.help       = "Affiche les informations d'une agence"
        this.arguments  = "<ID de l'agence>"
    }

    override fun execute(commandEvent: CommandEvent?) {
        val embedBuilder = EmbedBuilder()

        when (commandEvent!!.args.isEmpty()) {
            true -> {
                commandEvent.channel.sendMessage("${commandEvent.author.asMention}\n Tu dois spécifier l'ID de l'agence!").complete()
            }
            false -> {
                commandEvent.channel.sendMessage("${commandEvent.author.asMention}\n:inbox_tray: - Réception des données de l'agence spatiale...").complete()

                try {
                    val id = commandEvent.args
                    val agencyModel = AgenciesRepository().findOneByID(id)

                    val spaceCraftsString = if (agencyModel.spacecraft_list.size() > 0) {
                        makeStringFromJsonArray(agencyModel.spacecraft_list)
                    } else {
                        "Pas d'informations"
                    }

                    val launchersString = if (agencyModel.launcher_list.size() > 0) {
                        makeStringFromJsonArray(agencyModel.launcher_list)
                    } else {
                        "Pas d'informations"
                    }

                    embedBuilder.setTitle(agencyModel.name.noQuotes().toUpperCase())
                        .setThumbnail(agencyModel.logo_url.noQuotes())
                        .setImage(agencyModel.image_url.noQuotes())
                        .setColor(commandEvent.selfMember.color)
                        .addField("", agencyModel.description.noQuotes(), false)
                        .addBlankField(false)
                        .addField("Administrateur(s)", agencyModel.administrator.noQuotes(), true)
                        .addField("Type", agencyModel.type.noQuotes(), true)
                        .addField("Année de création", agencyModel.founding_year.noQuotes(), false)
                        .addBlankField(true)
                        .addField("Lanceurs", launchersString, false)
                        .addField("Vaisseaux", spaceCraftsString, false)
                        .addBlankField(true)
                        .addField("Tirs reussis", agencyModel.successful_launches.toString(), false)
                        .addField("Tirs ratés", agencyModel.failed_launches.toString(), false)
                        .addField("Tirs en attente", agencyModel.pending_launches.toString(), false)
                        .addBlankField(true)
                        .addField("Wiki", agencyModel.wiki_url, true)
                        .addField("Plus d'informations", agencyModel.info_url, true)
                        .addBlankField(true)

                    commandEvent.channel.sendMessage(embedBuilder.build()).complete()
                }
                catch (e: FileNotFoundException) {
                    commandEvent.channel.sendMessage("Je n'ai pas trouvé ce que tu recherches...").complete()
                }
            }
        }
    }

    private fun makeStringFromJsonArray(jsonArray: JsonArray) : String {
        val builder = StringBuilder()

        for (i in 0 until jsonArray.size()) {
            val spacecraft = jsonArray[i].asJsonObject
            val spaceCraftName = spacecraft["name"]
            val spacecraftID = spacecraft["id"]

            builder.append("[$spacecraftID] - ${spaceCraftName.toStringNoQuotes()}")
            builder.append("\n")
        }

        return builder.toString()
    }

}