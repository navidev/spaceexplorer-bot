package commands

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import extensions.noQuotes
import extensions.toStringNoQuotes
import net.dv8tion.jda.core.EmbedBuilder
import repositories.SpacecraftsRepository
import java.io.FileNotFoundException

class SpacecraftShowCommand : Command() {

    init {
        this.name       = "vaisseau"
        this.aliases    = arrayOf("vess")
        this.help       = "Affiche les informations d'un vaisseau"
        this.arguments  = "<ID du vaisseau>"
    }

    override fun execute(commandEvent: CommandEvent?) {
        val embedBuilder = EmbedBuilder()

        when (commandEvent!!.args.isEmpty()) {
            true -> commandEvent.channel.sendMessage("${commandEvent.author.asMention}\n Tu dois spécifier l'ID du vaisseau !").complete()
            else ->
            {
                try {

                    commandEvent.channel.sendMessage("${commandEvent.author.asMention}\n:inbox_tray: - Réception des données du vaisseau...").complete()

                    val id = commandEvent.args
                    val spacecraftModel = SpacecraftsRepository().findOneByID(id)
                    val spacecraftConfig = spacecraftModel.spacecraft_config.asJsonObject
                    val spacecraftHeight = when (spacecraftConfig["height"].toStringNoQuotes() != "null") {
                        true -> spacecraftConfig["height"].toStringNoQuotes()
                        false -> "N/C"

                    }
                    val spacecraftDiameter = when (spacecraftConfig["diameter"].toStringNoQuotes() != "null") {
                        true -> spacecraftConfig["diameter"].toStringNoQuotes()
                        false -> "N/C"

                    }
                    val isActive = when (spacecraftConfig["in_use"].toString()) {
                        "true" -> "Actuellement actif"
                        "false" -> "A la retraite"
                        else -> "N/C"
                    }

                    println(spacecraftConfig["image_url"].toStringNoQuotes())

                    embedBuilder.setTitle(spacecraftModel.name.noQuotes().toUpperCase())
                        .setImage(spacecraftConfig["image_url"].toStringNoQuotes())
                        .setColor(commandEvent.selfMember.color)
                        .addBlankField(false)
                        .addField("Statut", " ```$isActive``` ", true)
                        .addBlankField(false)
                        .addField("Description", spacecraftModel.description.noQuotes(), false)
                        .addBlankField(false)
                        .addField("Usage", spacecraftConfig["capability"].toStringNoQuotes(), false)
                        .addBlankField(false)
                        .addField("Histoire", spacecraftConfig["history"].toStringNoQuotes(), false)
                        .addBlankField(false)
                        .addField("Dimensions (en mètres)", "Hauteur : $spacecraftHeight | Diamètre : $spacecraftDiameter", false)
                        .addBlankField(false)

                    commandEvent.channel.sendMessage(embedBuilder.build()).complete()
                }
                catch (e: FileNotFoundException) {
                    commandEvent.channel.sendMessage("Je n'ai pas trouvé ce que tu recherches...").complete()
                }
            }
        }
    }
}