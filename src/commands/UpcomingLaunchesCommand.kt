package commands

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import conf.spaceAPIConf
import models.UpcomingLaunches
import extensions.toStringNoQuotes
import net.dv8tion.jda.core.EmbedBuilder
import java.io.FileNotFoundException
import java.net.URL

class UpcomingLaunchesCommand : Command() {

    init {
        this.name       = "lancements"
        this.aliases    = arrayOf("lan")
        this.help       = "Liste des prochains lancements"
        this.arguments  = ""
    }

    private lateinit var launch: JsonElement
    private lateinit var launchRocketArray: JsonElement
    private lateinit var launchRocketConfigArray: JsonElement
    private lateinit var launchStatusArray: JsonElement
    private lateinit var launchAgency: String
    private lateinit var launchStatusString: String
    private lateinit var launchID: String
    private lateinit var launchName: String
    private lateinit var launchDate: String

    override fun execute(commandEvent: CommandEvent?) {

        val embedBuilder = EmbedBuilder()

        commandEvent!!.channel.sendMessage("${commandEvent.author.asMention}\n:inbox_tray: - Réception des données des prochains lancements...").complete()

        try {
            val apiVersion = spaceAPIConf["API_VERSION"]
            val url = URL("https://spacelaunchnow.me/api/$apiVersion/launch/upcoming/?format=json").readText()
            val gson = Gson()
            val nextLaunchesModel = gson.fromJson(url, UpcomingLaunches::class.java)


            for (i in 0 until (nextLaunchesModel.results.size())) {
                launch = nextLaunchesModel.results[i]
                launchRocketArray = launch.asJsonObject["rocket"]
                launchRocketConfigArray = launchRocketArray.asJsonObject["configuration"]
                launchStatusArray = launch.asJsonObject["status"]
                launchAgency = launchRocketConfigArray.asJsonObject["launch_service_provider"].toStringNoQuotes()
                launchStatusString = launchStatusArray.asJsonObject["name"].toStringNoQuotes()
                launchID = launch.asJsonObject["id"].toStringNoQuotes()
                launchName = launch.asJsonObject["name"].toStringNoQuotes().toUpperCase()
                launchDate = launch.asJsonObject["net"].toStringNoQuotes()

                embedBuilder.setTitle(":rocket: - LISTE DES PROCHAINS LANCEMENTS")
                    .setColor(commandEvent.selfMember.color)
                    .addBlankField(false)
                    .addField(":small_red_triangle: $launchName", "Par **$launchAgency**\n"
                            + "Prévu le **${launchDate.substringBefore("T")}** à **${launchDate.substringAfter("T").substringBefore("Z")} UTC**\n"
                            + "Le statut est **$launchStatusString**\n\n"
                            + "Tape ``\$explore lancement $launchID`` pour plus d'informations sur ce lançement\n"
                        , false)
            }

            commandEvent.channel.sendMessage(embedBuilder.build()).complete()
        }
        catch (e: FileNotFoundException) {
            commandEvent.channel.sendMessage("Je n'ai pas trouvé ce que tu recherches...").complete()
        }

    }
}