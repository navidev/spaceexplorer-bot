package commands

import com.google.gson.*
import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import conf.spaceAPIConf
import models.Agencies
import extensions.toStringNoQuotes
import net.dv8tion.jda.core.EmbedBuilder
import java.io.FileNotFoundException
import java.net.URL

class AgenciesCommand : Command() {

    init {
        this.name       = "agences"
        this.aliases    = arrayOf("agcs")
        this.help       = "Liste des agences spatiales (Les plus populaires)"
        this.arguments  = ""
    }

    override fun execute(commandEvent: CommandEvent?) {
        val embedBuilder = EmbedBuilder()

        commandEvent!!.channel.sendMessage("${commandEvent.author.asMention}\n:inbox_tray: - Réception des données des agences spatiales...").complete()

        try {
            val apiVersion = spaceAPIConf["API_VERSION"]
            val url = URL("https://spacelaunchnow.me/api/$apiVersion/agencies/?featured=true&format=json").readText()
            val gson = Gson()
            val agenciesModel = gson.fromJson(url, Agencies::class.java)


            for (i in 0 until (agenciesModel.results.size())) {
                val agency = agenciesModel.results[i]
                val agencyID = agency.asJsonObject["id"].toStringNoQuotes()
                val agencyName = agency.asJsonObject["name"].toStringNoQuotes()
                val agencyAbbrev = agency.asJsonObject["abbrev"].toStringNoQuotes()
                val agencyType = agency.asJsonObject["type"].toStringNoQuotes()
                val agencyDescr = agency.asJsonObject["description"].toStringNoQuotes()
                val agencyCountryCode = agency.asJsonObject["country_code"].toStringNoQuotes()

                embedBuilder.setTitle(":department_store: - LISTE DES AGENCES (Les plus populaires)")
                    .setColor(commandEvent.selfMember.color)
                    .addBlankField(false)
                    .addField("- ${agencyName.toUpperCase()}",
                        " :id: $agencyID | :earth_americas: $agencyCountryCode"
                                + "\n"
                                + agencyDescr.substring(0..140)
                                + "...", false)
            }

            commandEvent.channel.sendMessage(embedBuilder.build()).complete()

        }
        catch (e: FileNotFoundException) {
            commandEvent.channel.sendMessage("Je n'ai pas trouvé ce que tu recherches...").complete()
        }
    }
}