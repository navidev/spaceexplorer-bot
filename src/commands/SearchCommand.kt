package commands

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import builders.SearchAgencyEmbedBuilder
import repositories.AgenciesRepository
import java.io.FileNotFoundException

class SearchCommand : Command() {

    init {
        this.name       = "codex"
        this.aliases    = arrayOf("f")
        this.help       = "Recherche une agence (pour le moment)"
        this.arguments  = "<nom de l'agence>"
    }

    override fun execute(commandEvent: CommandEvent?) {

        try {
            val search = commandEvent!!.args
            val searchResultModel = AgenciesRepository().searchByName(search)

            when (searchResultModel.results.size() > 0) {
                true -> {
                    val searchAgencyEmbed = SearchAgencyEmbedBuilder(searchResultModel, commandEvent.selfMember.color, commandEvent.args).makeEmbed()
                    commandEvent.channel.sendMessage(searchAgencyEmbed).complete()
                }
                false -> {
                    commandEvent.channel.sendMessage(" ${commandEvent.author.asMention} - Je n'ai pas trouvé de resultats pour **${commandEvent.args}**...").complete()
                }
            }

        }
        catch (e: FileNotFoundException) {
            commandEvent!!.channel.sendMessage("${commandEvent.author.asMention} - Je n'ai pas trouvé ce que tu recherches...").complete()
        }
    }
}